<?php

namespace Drupal\fluid_advagg\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

class FluidAdvAggController extends ControllerBase {

  public function serve_css($css_file) {

    if(!file_exists('sites/default/files/css')||!file_exists('sites/default/files/js')){
      drupal_set_message(t('Please ensure the following folders are created and are writable by Apache<br />sites/default/files/css<br />sites/default/files/js'), 'error');
    }

    $css_code = file_get_contents('sites/default/files/css/'.$css_file);;
    $response = new Response();
    $response->setContent($css_code);
    $response->headers->set('Content-Type', 'text/css');  
    $response->setStatusCode(Response::HTTP_OK);
    return $response;
    
  }

  public function serve_js($js_file) {

    $js_code = file_get_contents('sites/default/files/js/'.$js_file);;
    $response = new Response();
    $response->setContent($js_code);
    $response->headers->set('Content-Type', 'text/javascript');  
    $response->setStatusCode(Response::HTTP_OK);
    return $response;

  }

}
