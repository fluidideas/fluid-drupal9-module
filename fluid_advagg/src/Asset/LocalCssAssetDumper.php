<?php

namespace Drupal\fluid_advagg\Asset;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Asset\AssetDumper;
use tubalmartin\CssMin\Minifier as CSSmin;

/**
 * Dumps a CSS or JavaScript asset.
 */
class LocalCssAssetDumper extends AssetDumper {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * AssetDumper constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file handler.
   */
  public function __construct(FileSystemInterface $file_system = NULL) {
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   *
   * The file name for the CSS or JS cache file is generated from the hash of
   * the aggregated contents of the files in $data. This forces proxies and
   * browsers to download new CSS when the CSS changes.
   */
  public function dump($data, $file_extension) {
    $filename = $file_extension . '_' . Crypt::hashBase64($data) . '.' . $file_extension;
    $path = 'sites/default/files/' . $file_extension;
    $uri = $path . '/' . $filename;
    $data = $this->minifyCssMin($data);
    file_put_contents($uri, $data);
    file_put_contents($uri.'.gz', gzencode($data, 9, FORCE_GZIP));
    return $uri;
  }

  protected function minifyCssMin($contents) {
    $cssmin = new CSSmin(TRUE);
  
    // Minify the CSS splitting lines after 4k of text.
    $contents = $cssmin->run($contents, 4096);
  
    // Replaces @import commands with the actual stylesheet content.
    // This happens recursively but omits external files.
    $contents = preg_replace_callback('/@import\s*(?:url\(\s*)?[\'"]?(?![a-z]+:)(?!\/\/)([^\'"\()]+)[\'"]?\s*\)?\s*;/', [$this, 'loadNestedFile'], $contents);
  
    return $contents;
  }


  /**
   * Loads stylesheets recursively and returns contents with corrected paths.
   *
   * This function is used for recursive loading of stylesheets and
   * returns the stylesheet content with all url() paths corrected.
   *
   * @param array $matches
   *   An array of matches files to load.
   *
   * @return string
   *   The contents of the CSS file at $matches[1], with corrected paths.
   *
   * @see \Drupal\Core\Asset\CssOptimizer::loadFile()
   */
  protected function loadNestedFile(array $matches) {
    $filename = $matches[1];
    // Load the imported stylesheet and replace @import commands in there as
    // well.
    $file = $this->loadFile($filename);

    // Determine the file's directory.
    $directory = dirname($filename);
    // If the file is in the current directory, make sure '.' doesn't appear in
    // the url() path.
    $directory = $directory == '.' ? '' : $directory . '/';

    // Alter all internal url() paths. Leave external paths alone. We don't need
    // to normalize absolute paths here because that will be done later. Also
    // ignore SVG paths (# or %23).
    return preg_replace('/url\(\s*([\'"]?)(?![a-z]+:|\/+|\#|\%23+)([^\'")]+)([\'"]?)\s*\)/i', 'url(\1' . $directory . '\2\3)', $file);
  }

  /**
   * Loads the stylesheet and resolves all @import commands.
   *
   * Loads a stylesheet and replaces @import commands with the contents of the
   * imported file. Use this instead of file_get_contents when processing
   * stylesheets.
   *
   * The returned contents are compressed removing white space and comments only
   * when CSS aggregation is enabled. This optimization will not apply for
   * color.module enabled themes with CSS aggregation turned off.
   *
   * Note: the only reason this method is public is so color.module can call it;
   * it is not on the AssetOptimizerInterface, so future refactorings can make
   * it protected.
   *
   * @param string $file
   *   Name of the stylesheet to be processed.
   *
   * @return string
   *   Contents of the stylesheet, including any resolved @import commands.
   */
  public function loadFile($file) {
    $content = '';
    if ($contents = @file_get_contents($file)) {
      $contents = $this->clean($contents, []);
      $content = $this->optimize($contents, ['data' => $file], []);
      $this->addLicense($contents, $file);
    }

    return $content;
  }

}

