<?php

namespace Drupal\fluid_advagg\Asset;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Asset\AssetDumper;
use Drupal\Component\Utility\Unicode;


/**
 * Dumps a CSS or JavaScript asset.
 */
class LocalJsAssetDumper extends AssetDumper {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * AssetDumper constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file handler.
   */
  public function __construct(FileSystemInterface $file_system = NULL) {
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   *
   * The file name for the CSS or JS cache file is generated from the hash of
   * the aggregated contents of the files in $data. This forces proxies and
   * browsers to download new CSS when the CSS changes.
   */
  public function dump($data, $file_extension) {
    $filename = $file_extension . '_' . Crypt::hashBase64($data) . '.' . $file_extension;
    $path = 'sites/default/files/' . $file_extension;
    $uri = $path . '/' . $filename;
    $this->minifyJshrink($data);
    file_put_contents($uri, $data);
    file_put_contents($uri.'.gz', gzencode($data, 9, FORCE_GZIP));
    return $uri;
  }

  public function minifyJshrink(&$contents) {
    $contents_before = $contents;

    // Only include jshrink.inc if the JShrink\Minifier class doesn't exist.
    if (!class_exists('\JShrink\Minifier')) {
      include drupal_get_path('module', 'advagg_js_minify') . '/jshrink.inc';
      $nesting_level = ini_get('xdebug.max_nesting_level');
      if (!empty($nesting_level) && $nesting_level < 200) {
        ini_set('xdebug.max_nesting_level', 200);
      }
    }
    ob_start();
    try {
      // JShrink the contents of the aggregated file.
      // @codingStandardsIgnoreLine
      $contents = \JShrink\Minifier::minify($contents, ['flaggedComments' => FALSE]);

      // Capture any output from JShrink.
      $error = trim(ob_get_contents());
      if (!empty($error)) {
        throw new \Exception($error);
      }
    }
    catch (\Exception $e) {
      // Log the JSqueeze exception and rollback to uncompressed content.
      $contents = $contents_before.chr(10).chr(10).'/* '.chr(10).$e->getMessage().chr(10).' */';
    }
    ob_end_clean();
  }


}
