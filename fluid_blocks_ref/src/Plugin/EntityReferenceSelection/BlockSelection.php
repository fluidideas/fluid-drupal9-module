<?php
namespace Drupal\fluid_blocks_ref\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Component\Utility\Html;

/**
 * Filters the block listing for an entity reference field.
 *
 * @EntityReferenceSelection(
 *   id = "blocks",
 *   label = @Translation("Blocks: Filter by blocks in hidden region"),
 *   entity_types = {"block"},
 *   group = "blocks",
 *   weight = 1
 * )
 *
 */
class BlockSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {

    $query = $this->buildEntityQuery($match, $match_operator);
    if ($limit > 0) {
      $query->range(0, $limit);
    }

    $result = $query->execute();

    if (empty($result)) {
      return array();
    }

    //Create an array of blocks in hidden block region [block_config_entity_id] => [block_label]
    $options = array();
    $entities = $this->entityTypeManager->getStorage('block')->loadMultiple($result);
    foreach ($entities as $entity_id => $entity) {
      $block_region = $entity->getRegion();

      // Only add blocks that are in the hidden region of the main theme.
      if ($block_region == 'hidden') {
        $bundle = $entity->bundle();
        $options[$bundle][$entity_id] = $entity->label(); 
      }
    }

    //Sort the options alphabetically.
    if(!empty($options['block'])){
      asort($options['block']);
    }
    return $options;
  }

}
