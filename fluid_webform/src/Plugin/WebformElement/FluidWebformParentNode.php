<?php

namespace Drupal\fluid_webform\Plugin\WebformElement;

use Drupal\webform\WebformInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\TextBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'parent node' element which grabs the node id of the page your on.
 *
 * @WebformElement(
 *   id = "parent_node",
 *   api = "https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!Element!Hidden.php/class/Hidden",
 *   label = @Translation("Parent node"),
 *   description = @Translation("Provides a 'parent node' element which grabs the node id of the page your on."),
 *   category = @Translation("Basic elements"),
 * )
 */
class FluidWebformParentNode extends TextBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    return [
      // Element settings.
      'title' => 'Node id',
      'prepopulate' => FALSE
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preview() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTestValues(array $element, WebformInterface $webform, array $options = []) {
    // Hidden elements should never get a test value.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Remove the default section under the advanced tab.
    unset($form['default']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    $element['#value'] = self::getNodeId(); // set the node id here
    parent::prepare($element, $webform_submission);
  }

  /*
   * Function to grab the node Id of the current page
   */

  public function getNodeId(){
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      return $node->id();
    }
    return '';
  }
}
