<?php

namespace Drupal\fluid_webform\Element;

use Drupal\Core\Render\Element\Hidden;

/**
 * Provides a hidden form element for the fluid webform parent node element
 *
 * @FormElement("parent_node")
 */
class FluidWebformParentNode extends Hidden {

}