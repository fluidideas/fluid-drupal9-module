<?php

/**
 * @file
 * Batch API Methods.
 */

/**
 * Batch method to load node and check for para field content.
 *
 * @param array $nids
 *   Node ids to process per batch run.
 * @param array $context
 *   Batch param.
 */
function batch_get_para_fields(array $nids, array &$context) {
  $paraReport = \Drupal::service('paragraphs_report.report');
  // Initiate multistep processing and any custom batch vars.
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($nids);
  }
  // Counter var.
  if (empty($context['results']['count'])) {
    $context['results']['count'] = 0;
  }
  if (!empty($nids)) {
    foreach ($nids as $nid) {
      $current = isset($context['results']['paras']) ? $context['results']['paras'] : [];
      $context['results']['paras'] = $paraReport->getParasFromNid($nid, $current);
      $context['results']['count']++;
      $context['sandbox']['progress']++;
    }
    // Multistep processing : report progress.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }
}

/**
 * After batch process has completed, save results to db.
 *
 * @param bool $success
 *   Check if batch process completed without errors.
 * @param array $results
 *   Batch result data.
 * @param array $operations
 *   Batch operations.
 */
function batch_save(bool $success, array $results, array $operations) {
  // JSON encode and save to module config.
  $paraReport = \Drupal::service('paragraphs_report.report');
  $paraReport->configSaveReport((array)$results['paras']);
  if ($success) {
    $message = \Drupal::translation()
      ->formatPlural($results['count'], 'One node processed.', '@count nodes processed.');
    \Drupal::logger('Paragraphs Report')->notice($message);
    \Drupal::messenger()->addStatus($message);
  }
  else {
    $message = t('Finished with an error.');
    \Drupal::logger('Paragraphs Report')->error($message);
    \Drupal::messenger()->addError($message);
  }
}
