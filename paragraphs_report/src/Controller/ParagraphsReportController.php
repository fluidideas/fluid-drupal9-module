<?php

namespace Drupal\paragraphs_report\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paragraphs_report\ParagraphsReport;

/**
 * Paragraphs Report controller.
 */
class ParagraphsReportController extends ControllerBase {

  /**
   * ParagraphsReport class object.
   *
   * @var \Drupal\paragraphs_report\ParagraphsReport
   */
  protected $paragraphsReport;

  /**
   * Constructs the controller object.
   *
   * @param \Drupal\paragraphs_report\ParagraphsReport $paragraphsReport
   *   Paragraphs report object.
   */
  public function __construct(ParagraphsReport $paragraphsReport) {
    $this->paragraphsReport = $paragraphsReport;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('paragraphs_report.report')
    );
  }

  /**
   * Return a rendered table ready for output.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Return markup for report output.
   */
  public function showReport() {
    return $this->paragraphsReport->showReport();
  }

  /**
   * Batch API starting point.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Return redirect after batch has completed.
   */
  public function batchRunReport() {
    return $this->paragraphsReport->batchSetup();
  }

  /**
   * Return a CSV file of the rendered data.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function exportReport() {
    return $this->paragraphsReport->exportReport();
  }

}
