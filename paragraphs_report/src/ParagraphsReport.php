<?php

namespace Drupal\paragraphs_report;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Xss;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\path_alias\AliasManager;
use Drupal\Core\Pager\PagerManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Class ParagraphsReport.
 *
 * Control various module logic like batches, lookups, and report output.
 *
 * @package Drupal\paragraphs_report
 */
class ParagraphsReport {

  use StringTranslationTrait;

  /**
   * Configuration settings.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Current path.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Alias manager.
   *
   * @var Drupal\path_alias\AliasManager
   */
  protected $aliasManager;

  /**
   * Class property to get config settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $paraSettings;

  /**
   * Class property to set config settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $paraEditSettings;

  /**
   * Pager class.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * Constructs the controller object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration settings.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   Field manager.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPath
   *   Current path.
   * @param Drupal\path_alias\AliasManager $aliasManager
   *   Alias manager.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pagerManager
   *   Pager manager.
   */
  public function __construct(ConfigFactoryInterface $configFactory,
                              EntityFieldManager $entityFieldManager,
                              EntityTypeManager $entityTypeManager,
                              CurrentPathStack $currentPath,
                              AliasManager $aliasManager,
                              PagerManagerInterface $pagerManager) {
    $this->configFactory = $configFactory;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentPath = $currentPath;
    $this->aliasManager = $aliasManager;
    $this->pagerManager = $pagerManager;

    $this->paraSettings = $this->configFactory->get('paragraphs_report.settings');
    $this->paraEditSettings = $this->configFactory->getEditable('paragraphs_report.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('path.current'),
      $container->get('path_alias.manager'),
      $container->get('pager.manager')
    );
  }

  /**
   * Batch API process starting point.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   Batch process response redirect returned.
   */
  public function batchSetup() {
    // Get all nodes to process.
    $nids = $this->getNodes();
    // Put nodes into batches.
    $batch = $this->batchPrep($nids);
    // Start batch api process.
    batch_set($batch);
    // Redirect page and display message on completion.
    return batch_process('/admin/reports/paragraphs-report');
  }

  /**
   * Setup batch array var.
   *
   * @param array $nids
   *   Node ids.
   *
   * @return array
   *   Batches ready to run
   */
  public function batchPrep(array $nids = []) {
    $totalRows = count($nids);
    $rowsPerBatch = $this->paraSettings->get('import_rows_per_batch') ?: 10;
    $batchesPerImport = ceil($totalRows / $rowsPerBatch);
    // Put x-amount of rows into operations array slots.
    $operations = [];
    for ($i = 0; $i < $batchesPerImport; $i++) {
      $offset = ($i == 0) ? 0 : $rowsPerBatch * $i;
      $batchNids = array_slice($nids, $offset, $rowsPerBatch);
      $operations[] = ['batch_get_para_fields', [$batchNids]];
    }
    // Full batch array.
    $batch = [
      'init_message' => $this->t('Executing a batch...'),
      'progress_message' => $this->t('Operation @current out of @total batches, @perBatch per batch.',
        ['@perBatch' => $rowsPerBatch]
      ),
      'progressive' => TRUE,
      'error_message' => $this->t('Batch failed.'),
      'operations' => $operations,
      'finished' => 'batch_save',
      'file' => drupal_get_path('module', 'paragraphs_report') . '/paragraphs_report.batch.inc',
    ];
    return $batch;
  }

  /**
   * Get paragraph fields from a bundle/type.
   *
   * @param string $bundle
   *   Entity bundle like 'node'.
   * @param string $type
   *   Entity type like 'page'.
   *
   * @return array
   *   Array of paragraph fields on a node.
   */
  public function getParaFieldsOnType($bundle = '', $type = '') {
    $paraFields = [];
    $fields = $this->entityFieldManager->getFieldDefinitions($bundle, $type);
    foreach ($fields as $field_name => $field_definition) {
      if (!empty($field_definition->getTargetBundle()) && $field_definition->getSetting('target_type') == 'paragraph') {
        $paraFields[] = $field_name;
      }
    }
    return $paraFields;
  }

  /**
   * Get paragraph fields for selected content types.
   *
   * @return array
   *   Paragraph fields by content type key
   */
  public function getParaFieldDefinitions() {
    // Loop through the fields for chosen content types to get paragraph fields.
    // Example content_type[] = field_name.
    $paraFields = [];
    foreach ($this->getTypes() as $contentType) {
      $fields = $this->entityFieldManager->getFieldDefinitions('node', $contentType);
      foreach ($fields as $field_name => $field_definition) {
        if (!empty($field_definition->getTargetBundle()) && $field_definition->getSetting('target_type') == 'paragraph') {
          $paraFields[$contentType][] = $field_name;
        }
      }
    }
    return $paraFields;
  }

  /**
   * Get list of content types chosen from settings.
   *
   * @return array
   *   Array of content types labels.
   */
  public function getTypes() {
    $data = $this->paraSettings->get('content_types') ?? [];
    return array_filter($data);
  }

  /**
   * Query db for nodes to check for paragraphs.
   *
   * @return array|int
   *   Nids to check for para fields or empty.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNodes() {
    $contentTypes = array_filter($this->paraSettings->get('content_types'));
    $query = $this->entityTypeManager->getStorage('node');
    $nids = $query->getQuery()
      ->condition('type', $contentTypes, 'IN')
      ->execute();
    return $nids;
  }

  /**
   * Pass node id, return paragraphs report data as array.
   *
   * @param string $nid
   *   Node id.
   * @param array $current
   *   Paragraphs to append new ones to.
   *
   * @return array
   *   Array of paragraphs report data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getParasFromNid($nid = '', array $current = []) {
    // Pass any current array items into array.
    $arr = $current;
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    // Get and loop through first level paragraph fields on the node.
    $paraFields = $this->getParaFieldsOnType('node', $node->bundle());
    foreach ($paraFields as $paraField) {
      // Get paragraph values (target_ids).
      $paras = $node->get($paraField)->getValue();
      foreach ($paras as $para) {
        // Load paragraph from target_id.
        $p = Paragraph::load($para['target_id']);
        // Only add the paragraph if it has loaded successfully.
        if (!empty($p)) {
          $arr[$p->bundle()]['top'][] = $nid;
          // Check if the top level paragraph has sub-paragraph fields.
          $arr = $this->getParaSubFields($node, $p, $arr);
        }
      }
    }
    return $arr;
  }

  /**
   * Helper recursive method to find embedded paragraphs.
   *
   * Send a paragraph, check fields for sub-paragraph fields recursively.
   *
   * @param object $node
   *   Node to parse.
   * @param object $paragraph
   *   Paragraph object to parse.
   * @param array $reports
   *   Array to store parsed paragraph report data.
   *
   * @return array
   *   Paragraph values.
   */
  public function getParaSubFields($node, $paragraph, array $reports) {
    // Get fields on paragraph and check field type.
    $fields = $this->entityFieldManager->getFieldDefinitions('paragraph', $paragraph->bundle());
    foreach ($fields as $field_name => $field_definition) {
      // Check if this field a paragraph type.
      if (!empty($field_definition->getTargetBundle()) && $field_definition->getSetting('target_type') == 'paragraph') {
        // Get paragraphs on this field.
        $paras = $paragraph->get($field_name)->getValue();
        foreach ($paras as $para) {
          $p = Paragraph::load($para['target_id']);
          // If yes, add this field to report and check for more sub-fields.
          // Example arr[main component][parent] = alias of node.
          $reports[$p->bundle()][$paragraph->bundle()][] = $node->id();
          $reports = $this->getParaSubFields($node, $p, $reports);
        }
      }
    }
    return $reports;
  }

  /**
   * Get a list of the paragraph components and return as lookup array.
   *
   * @return array
   *   Machine name => label.
   */
  public function getParaTypes() {
    $paras = paragraphs_type_get_types();
    $names = [];
    foreach ($paras as $machine => $obj) {
      $names[$machine] = $obj->label();
    }
    return $names;
  }

  /**
   * Pass array of path data to save for the report.
   *
   * @param array $arr
   *   Paragraph->parent->path data.
   */
  public function configSaveReport(array $arr = []) {
    $json = Json::encode($arr);
    $this->paraEditSettings->set('report', $json)->save();
  }

  /**
   * Remove a node path from report data.
   *
   * @param string $removeNid
   *   Remove from report data.
   *
   * @return array
   *   Updated encoded data.
   */
  public function configRemoveNode($removeNid = '') {
    $json = Json::decode($this->paraSettings->get('report'));
    // Force type to be array.
    $json = is_array($json) ? $json : [];
    // Search for nid and remove from array.
    // Remove item from array.
    $new = [];
    foreach ($json as $para => $sets) {
      foreach ($sets as $parent => $nids) {
        // Remove nid from array.
        $tmp = [];
        foreach ($nids as $nid) {
          if ($nid != $removeNid) {
            $tmp[] = $nid;
          }
        }
        $new[$para][$parent] = $tmp;
      }
    }
    // Save updated array.
    $this->configSaveReport($new);
    return $new;
  }

  /**
   * Check that node meets conditions to use in report data.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object to check.
   *
   * @return bool
   *   True or false if content type of entity should be watched.
   */
  public function checkWatch(EntityInterface $entity) {
    if ($this->paraSettings->get('watch_content')
      && in_array($entity->bundle(), $this->getTypes())) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Add report data from new node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Node to check.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function insertParagraphs(EntityInterface $entity) {
    if ($this->checkWatch($entity)) {
      // Send node to get parsed for paragraph fields/sub-fields.
      $json = Json::decode($this->paraSettings->get('report'));
      $updated = $this->getParasFromNid($entity->id(), $json);
      $this->configSaveReport($updated);
    }
  }

  /**
   * Update report data with paragraph changes in node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Node to check.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function updateParagraphs(EntityInterface $entity) {
    if ($this->checkWatch($entity)) {
      // Send node to get parsed for paragraph fields/sub-fields.
      $json = $this->configRemoveNode($entity->id());
      $updated = $this->getParasFromNid($entity->id(), $json);
      $this->configSaveReport($updated);
    }
  }

  /**
   * Remove deleted node path from report data.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Node to check.
   */
  public function deleteParagraphs(EntityInterface $entity) {
    if ($this->checkWatch($entity)) {
      $json = $this->configRemoveNode($entity->id());
      $this->configSaveReport($json);
    }
  }

  /**
   * Build quick paragraphs type drop down form.
   *
   * @return string
   *   HTML used for select form element.
   */
  public function filterForm() {
    // Build filter form.
    // Check and set filters.
    $paraNames = $this->getParaTypes();
    $current_path = $this->currentPath->getPath();
    $filterForm = '<form method="get" action="' . $current_path . '">';
    $filterForm .= 'Filter by Type: <select name="ptype">';
    $filterForm .= '<option value="">All</option>';
    foreach ($paraNames as $machine => $label) {
      $selected = isset($_GET['ptype']) && $_GET['ptype'] == $machine ? ' selected' : '';
      $filterForm .= '<option name="' . $machine . '" value="' . $machine . '"' . $selected . '>' . $label . '</option>';
    }
    $filterForm .= '</select> <input type="submit" value="Go"></form><br>';
    return $filterForm;
  }

  /**
   * Format the stored JSON config var into a rendered table.
   *
   * @param array $json
   *   Stored paragraph report data.
   *
   * @return array
   *   Table render array.
   */
  public function formatTable($json = []) {
    // Parse the JSON into tabular data.
    $tabular = $this->getTabularData($json);
    // Setup pager.
    $per_page = 10;
    $current_page = $this->pagerManager
      ->createPager($tabular['total'], $per_page)
      ->getCurrentPage();
    // Split array into page sized chunks, if not empty.
    $chunks = !empty($tabular['rows']) ? array_chunk($tabular['rows'], $per_page, TRUE) : 0;
    // Table output.
    $table['table'] = [
      '#type' => 'table',
      '#title' => $this->t('Paragraphs Report'),
      '#header' => $tabular['header'],
      '#sticky' => TRUE,
      '#rows' => $chunks[$current_page],
      '#empty' => $this->t('No components found. You may need to run the report.'),
    ];
    $table['pager'] = [
      '#type' => 'pager',
    ];
    return $table;
  }

  /**
   * Return a rendered table ready for output.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   Table markup returned.
   */
  public function showReport() {
    // Build report from stored JSON in module config.
    $btn['run_button'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<div style="float:right"><a class="button" href="/admin/reports/paragraphs-report/update" onclick="return confirm(\'Update the report data with current node info?\')">Update Report Data</a></div>'),
    ];
    $btn['export_button'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<div style="float:right; margin-right: 20px;">' . $this->getExportLink()->toString() . '</div>'),
    ];
    $json = $this->fetchJson();
    $filters = [];
    $filters['filter'] = [
      '#type' => 'markup',
      '#markup' => $this->filterForm(),
      '#allowed_tags' => array_merge(Xss::getHtmlTagList(),
        ['form', 'option', 'select', 'input', 'br']),
    ];
    $table = $this->formatTable($json);
    return [
      $btn,
      $filters,
      $table,
    ];
  }

  /**
   * Generate the export link.
   *
   * @return \Drupal\Core\Link
   *   Returns a Link object for the export.
   */
  protected function getExportLink($include_filters = TRUE) {
    $attributes = ['class' => ['button']];
    $params = [];
    if ($include_filters) {
      $params = \Drupal::request()->query->all();
    }
    $url = Url::fromRoute('paragraphs_report.export', $params, ['attributes' => $attributes]);
    return Link::fromTextAndUrl('Export to CSV', $url);
  }

  /**
   * Fetches the store JSON data.
   *
   * @return array
   *   Returns an array of JSON data.
   */
  protected function fetchJson() {
    $json = Json::decode($this->paraSettings->get('report'));

    // Force type to be array.
    return is_array($json) ? $json : [];
  }

  /**
   * Parses the JSON data into tabular data.
   *
   * @param array $json
   *   Stored paragraph report data.
   *
   * @return array
   *   Returns an array of tabular data.
   */
  protected function getTabularData($json = [], $show_links = TRUE) {
    $paraNames = $this->getParaTypes();
    $filter = isset($_GET['ptype']) ? trim($_GET['ptype']) : '';

    // Get paragraphs label info, translate machine name to label.
    // Loop results into the table.
    $total = 0;
    $rows = [];

    if (!empty($json)) {
      foreach ($json as $name => $set) {
        // Skip if we are filtering out all but one.
        if (!empty($filter) && $filter != $name) {
          continue;
        }
        // Be mindful of the parent field.
        foreach ($set as $parent => $nids) {
          // Turn duplicates into counts.
          if (!empty($nids)) {
            $counts = array_count_values($nids);
            foreach ($counts as $nid => $count) {
              $alias = $this->aliasManager->getAliasByPath('/node/' . $nid);
              $link = $this->t('<a href="@alias">@alias</a>', ['@alias' => $alias]);
              $label = $paraNames[$name];
              $rows[] = [$label, $parent, ($show_links ? $link : $alias), $count];
              $total++;
            }
          }
        }
      }
    }

    $header = [
      $this->t('Paragraph'),
      $this->t('Parent'),
      $this->t('Path'),
      $this->t('Count'),
    ];

    return compact('header', 'rows', 'total');
  }

  /**
   * Returns a CSV file with rendered data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns a CSV file for download.
   */
  public function exportReport() {
    // Fetch the JSON data.
    $json = $this->fetchJson();

    // Package up the data into rows for the CSV.
    $export_data = $this->getTabularData($json, FALSE);

    // Serve the CSV file.
    return $this->serveExportFile($export_data);
  }

  /**
   * Generates the file and returns the response.
   *
   * @param array $data
   *   A multidimensional array containing the CSV data.
   * @param string $filename
   *   The name of the file to create. Will auto-generate a filename if none is provided.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns a Response object formatted for the CSV.
   */
  protected function serveExportFile($data, $filename = NULL) {
    if (empty($filename) || !is_string($filename)) {
      $filename = 'paragraphs-report-export--' . date('Y-M-d-H-i-s') . '.csv';
    }

    $formatted_data = $this->formatExportData($data);
    if (empty($formatted_data)) {
      // @todo Add in some error handling and possibly watchdog warnings.
      $formatted_data = '';
    }

    // Generate the CSV response to serve up to the browser.
    $response = new Response();
    $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
    $response->headers->set('Content-Disposition', $disposition);
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Expires', 0);
    $response->headers->set('Content-Transfer-Encoding', 'binary');
    $response->setContent($formatted_data);

    return $response;
  }

  /**
   * Convert a multidimensional array into a string for CSV export purposes.
   *
   * @param array $raw
   *   A multidimensional array containing the CSV data.
   *
   * @return string|boolean
   *   Returns the data compacted into a single string for CSV export purposes. If the raw data was not an array or empty, returns FALSE.
   */
  protected function formatExportData($raw) {
    if (!is_array($raw) || empty($raw)) {
      return FALSE;
    }

    $data = [];
    if (!empty($raw['header'])) {
      $data[] = '"' . implode('","', $raw['header']) . '"';
    }

    foreach ($raw['rows'] as $key => $row) {
      if (is_array($row)) {
        $data[] = '"' . implode('","', $row) . '"';
      }
    }

    return implode("\n", $data);
  }

}
