This module creates a boolean field attached to the user entity type.

Once you put your credentials in the config page, this field will subscribe/unsubscribe users to your Mailchimp mailing list.

You need to take some extra steps to get this working.

1.) Run composer require mailchimp/marketing to get the PHP library.

2.) The field "mailchimp" is disabled by default. You need to go to /admin/config/people/accounts/form-display and move the field out of the disabled category.

3.) Complete the info in the config page.
