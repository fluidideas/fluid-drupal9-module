<?php

namespace Drupal\fluid_mailchimp;

use Drupal\user\Entity\User;

/**
 * Class MailchimpService.
 */
class MailchimpService {

  private $config;

  /**
   * Constructs a new MailchimpService object.
   */
  public function __construct() {
    $this->config = \Drupal::config('fluid_mailchimp.config');
  }

  /**
   * Connect to Mailchimp
   */
  public function connect() {

    $mailchimp = new \MailchimpMarketing\ApiClient();

    $mailchimp->setConfig([
      'apiKey' => $this->config->get('api_key'),
      'server' => $this->config->get('server_prefix'),
    ]);

    return $mailchimp;
  }

  /**
   * Check user is subscribed
   * 
   * Returns 'subscribed', 'pending', 'unsubscribed', or 'nosub'.
   */
  public function check(User $user) {

    $mailchimp = $this->connect();
    $email = $user->get('mail')->value;
    $list_id = $this->config->get('list_id');
    $subscriber_hash = md5(strtolower($email));

    try {
      $response = $mailchimp->lists->getListMember($list_id, $subscriber_hash);
      return $response->status;
    } catch (\Exception $e) {
      return 'nosub';
    }
  }

  /**
   * Subscribe a user
   */
  public function sub(User $user) {

    $mailchimp = $this->connect($user);
    $list_id = $this->config->get('list_id');

    //try {
      $mailchimp->lists->addListMember($list_id, [
        "email_address" => $user->get('mail')->value,
        "status" => "subscribed",
        "merge_fields" => [
          "FNAME" => $user->get('field_first_name')->value,
          "LNAME" => $user->get('field_last_name')->value,
        ],
      ]);
    //} catch (\Exception $e) {
    //  $message = $e->getMessage();
    //  \Drupal::logger('fluid_mailchimp')->log("SUB ERROR");
    //  \Drupal::logger('fluid_mailchimp')->log($message);
    //  \Drupal::messenger()->addError('Something went wrong. We were unable to subscribe you to our mailing list. Please email ' . \Drupal::config('system.site')->get('mail') . ' if this problem continues.');
    //}

    \Drupal::messenger()->addStatus('Thank you for subscribing to our mailing list!');
  }

  /**
   * Unsubscribe a user
   */
  public function unsub(User $user) {

    $mailchimp = $this->connect();
    $email = $user->get('mail')->value;
    $list_id = $this->config->get('list_id');
    $subscriberHash = md5(strtolower($email));

    try {
      $mailchimp->lists->updateListMember($list_id, $subscriberHash, ["status" => "unsubscribed"]);
      \Drupal::messenger()->addStatus('You have unsubscribed from our mailing list.');
    } catch (\Exception $e) {
      $message = $e->getMessage();
      \Drupal::logger('fluid_mailchimp')->log("UNSUB ERROR");
      \Drupal::logger('fluid_mailchimp')->log($message);
      \Drupal::messenger()->addError('Something went wrong. We were unable to unsubscribe you from our mailing list. Please email ' . \Drupal::config('system.site')->get('mail') . ' if this problem continues.');
    }
  }

  /**
   * Delete a user
   */
  public function delete(User $user) {

    $mailchimp = $this->connect();
    $email = $user->get('mail')->value;
    $list_id = $this->config->get('list_id');
    $subscriberHash = md5(strtolower($email));

    try {
      $mailchimp->lists->deleteListMemberPermanent($list_id, $subscriberHash);
      \Drupal::messenger()->addStatus('You have been deleted from our mailing list.');
    } catch (\Exception $e) {
      $message = $e->getMessage();
      \Drupal::logger('fluid_mailchimp')->log("DELETE ERROR");
      \Drupal::logger('fluid_mailchimp')->log($message);
      \Drupal::messenger()->addError('Something went wrong. We were unable to delete you from our mailing list. Please email ' . \Drupal::config('system.site')->get('mail') . ' if this problem continues.');
    }
  }

  /**
   * Resubscribe a user (sends a confirmation email)
   */
  public function resub(User $user) {

    $mailchimp = $this->connect();
    $email = $user->get('mail')->value;
    $list_id = $this->config->get('list_id');
    $subscriberHash = md5(strtolower($email));

    try {
      //We're not allowed to resubscribe users via the api for GDPR reasons. We can set them as pending.
      $mailchimp->lists->updateListMember($list_id, $subscriberHash, ["status" => "pending"]);
      \Drupal::messenger()->addStatus('Thank you for subscribing! You have been sent an email to confirm your subscription.');
    } catch (\Exception $e) {
      $message = $e->getMessage();
      \Drupal::logger('fluid_mailchimp')->log("RESUB ERROR");
      \Drupal::logger('fluid_mailchimp')->log($message);
      \Drupal::messenger()->addError('Something went wrong. We were unable to resubscribe you to our mailing list. Please email ' . \Drupal::config('system.site')->get('mail') . ' if this problem continues.');
    }
  }
}

