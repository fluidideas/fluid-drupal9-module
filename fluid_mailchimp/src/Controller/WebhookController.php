<?php

namespace Drupal\fluid_mailchimp\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WebhookController.
 */
class WebhookController extends ControllerBase {

  /**
   * Webhook.
   *
   * @return string
   *   Return Hello string.
   */
  public function webhook($hash) {

    $config = \Drupal::config('fluid_mailchimp.config');
    $webhook = $config->get('webhook');

    //Check if the url uses the correct hash
    if ($webhook != $hash) {
      $response = new Response(
        0,
        Response::HTTP_FORBIDDEN,
        ['content-type' => 'text/plain']
      );
      return $response;
    } else {

      //Are we dealing with a unsubscribe click?
      if ($_POST['type'] == 'unsubscribe') {

        //Alter user account
        \Drupal::logger('fluid_mailchimp')->notice($_POST['data']['email']);
        $user = user_load_by_mail($_POST['data']['email']);
        $user->set('field_mailchimp', "0");
        $user->save();
      }

      $response = new Response(
        1,
        Response::HTTP_OK,
        ['content-type' => 'text/plain']
      );
      return $response;
    }
  }

}
