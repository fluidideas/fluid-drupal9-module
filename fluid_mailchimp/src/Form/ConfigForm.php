<?php

namespace Drupal\fluid_mailchimp\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm
 */
class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fluid_mailchimp.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('fluid_mailchimp.config');
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('Enter your Mailchimp API key'),
      '#maxlength' => 200,
      '#size' => 64,
      '#default_value' => $config->get('api_key'),
    ];
    $form['server_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server prefix'),
      '#description' => $this->t('The first subdomain in the url of your Mailchimp dashboard E.g. \'us2\'.'),
      '#maxlength' => 200,
      '#size' => 64,
      '#default_value' => $config->get('server_prefix'),
    ];
    $form['list_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Audience ID'),
      '#description' => $this->t('This is not the name of the audience, it\'s the unique ID. This is awkward to find in Mailchimp. Just do a Google search.'),
      '#maxlength' => 200,
      '#size' => 64,
      '#default_value' => $config->get('list_id'),
    ];
    $form['webhook'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook secret'),
      '#description' => $this->t('Can be anything you like. Use: ' . \Drupal::request()->getSchemeAndHttpHost() . '/fluid_mailchimp/webhook/{hash} as the Mailchimp webhook URL.'),
      '#maxlength' => 200,
      '#size' => 64,
      '#default_value' => $config->get('webhook'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('fluid_mailchimp.config')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('server_prefix', $form_state->getValue('server_prefix'))
      ->set('list_id', $form_state->getValue('list_id'))
      ->set('webhook', $form_state->getValue('webhook'))
      ->save();
  }

}
