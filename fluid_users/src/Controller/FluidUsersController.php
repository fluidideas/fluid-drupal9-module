<?php

namespace Drupal\fluid_users\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\user\Entity\User;

/**
 * Class FluidUsersController.
 */
class FluidUsersController extends ControllerBase {


    public function content() {

        // Get master list of users from Concept
        $markup = 'Running user import and synchronisation.<br />';
        $feed = json_decode(file_get_contents('https://concept.fluid-dev.co.uk/users.php'), true);
        if(count($feed) > 0){
            // First let's block all fluid users
            $sql = "SELECT u.uid, u.mail FROM users_field_data AS u WHERE u.mail LIKE :mail AND u.uid != :uid";
            $result = \Drupal::database()->query($sql, [':mail' => '%@fluid-ideas.co.uk', ':uid' => 1]);
            while($row = $result->fetchObject()){
                if(!in_array($row->mail, $feed)){
                    $user = User::load($row->uid);
                    $user->block();
                    $user->save();
                }
            }
            foreach($feed as $user){
                // Check if user exists
                if(user_load_by_mail($user)){
                    $markup .= 'Found: '.$user.'<br />';
                    $user = user_load_by_mail($user);
                    $user->activate();
                    $user->save();
                } else {
                    $markup .= 'New user: '.$user.'<br />';
                    $email = $user;
                    $name = explode('@', $user);
                    $user = User::create();
                    $user->setPassword(user_password());
                    $user->enforceIsNew();
                    $user->setEmail($email);
                    $user->setUsername($name[0]);
                    $user->addRole('administrator');
                    $user->activate();
                    $user->save();
                }
            }
            // Set user 1 to random password
            $user = User::load(1);
            $user->setPassword(user_password());
            $user->save();
        }
        return [
            '#type' => 'markup',
            '#markup' => $markup
        ];
  

    }


}