<?php

namespace Drupal\fluid_get_field\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\user\Entity\User;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\Entity\Term;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;


/**
 * Class to provide field data for preprocess funcitons in a structures way
 */
class FluidGetFieldController extends ControllerBase {

  /*
   * Function that process the rows and fields at view level
   */
  public static function process_view(&$variables) {
    //Setup the fluid varible to attch rows and fields
    $variables['fluid'] = [
      'rows' => []
    ];

    $view = $variables['view'];
    $rows = $variables['rows'];
    $fields = $view->field;

    $fluidRows = [];

    //loop though all rows on the view
    foreach ($rows as $rowId => $row) {
      $rowEntity = $row["content"]["#row"]->_entity;
      $fluidFields = new fluidRow();

      // loop though all field in each row in the view.
      foreach ($fields as $fieldId => $field) {
        if ($rowEntity->hasField($fieldId)){
          $fluidFieldValue = new fluidField($rowEntity->get($fieldId));
          $fluidFields->createProperty($fieldId, $fluidFieldValue);
        }
      }

      array_push($fluidRows, $fluidFields);
    }

    //attach the array of rows and fields to the fluid object
    $variables['fluid']['rows'] = $fluidRows;
  }


  /*
   * Function that process the fields and results at field level
   */
  public static function process_field(&$variables) {

    $view = $variables['view'];
    $rows = $view->result;
    $fields = $view->field;

    $row = $variables['row'];
    $field = $variables['field'];
    $fieldId = $field->field;

    $fieldTypes = [
      ''
    ];

    $fluidRows = [];

    if ($fieldId != 'node_bulk_form'){
      if($row->_entity){
        if ($row->_entity->hasField($fieldId)){
          $fluidFieldValue = new fluidField($row->_entity->get($fieldId));

          //attach the helper class of the field to the fluid object so it can be access using the field name or .field property
          $variables['fluid'] = [
            $fieldId => $fluidFieldValue,
            'field' => $fluidFieldValue
          ];

          //loop though all results on the view
          foreach ($rows as $rowId => $row) {
            $rowEntity = $row->_entity;
            $fluidFields = new fluidRow();

            if (!is_null($rowEntity)) {
              //kint($fields);
              //kint($rowEntity->getFields(false));
              //die();
              $rowFields = $rowEntity->getFields(false);
              // loop though all field in each result in the view.
              foreach ($rowFields as $fieldId2 => $field) {
                //dump("fieldID2: " . $fieldId2);

                if ($rowEntity->hasField($fieldId2)){
                  $fluidFieldValue = new fluidField($rowEntity->get($fieldId2));
                  $fluidFields->createProperty($fieldId2, $fluidFieldValue);
                }
              }

              array_push($fluidRows, $fluidFields);
            }
          }
        }

        //attach the array of rows and fields to the fluid object
        $variables['fluid']['rows'] = $fluidRows;
      }
    }

  }


  /*
   * Function that process the fields and results at field level
   */
  public static function process_node(&$variables) {

//    dump("I'm Processing Nodes");

    $node = $variables['node'];
    if($node->body){
      $fluidFieldValue = new fluidField($node->get('body'));
      $variables['fluid'] = [
        'body' => $fluidFieldValue,
      ];
    }

    foreach($node as $key => $var){
      if(substr($key, 0, 6) == 'field_'){
        $fluidFieldValue = new fluidField($node->get($key));
        $variables['fluid'][$key] = $fluidFieldValue;
      }
    }

  }

}








/* A custom class that represnts a field in a view with helper
 * functions to simplfy usage in twig */
class fluidField {
  private $_raw;
  private $_properties = [];

  public function __get($name) {
    if (method_exists($this, ($method = 'get' . $name))) {
      return $this->$method();
    } else return;
  }

  /*
   * Function that returns an number of values in the field
   */
  public function getCount(){
      $fieldContents = $this->_raw;

      return count($fieldContents); //$this->_raw->list->count();
  }

  /*
   * Function that returns the contents of the field - if the field has multiple
   * values then they will be returned as a commer separted list.
   *
   * * @param int $iStart
   *   [Optional] Pass through the index of the field value
   * * @param object $options
   *   [Optional] Pass through options for the field i.e. {dateFormat: 'd/m/Y H:i'}
   *      outputType  = "rendered" - render html output,
   *                  = "plan"     - render field value
   *      class       - when using rendered output you can pass though classes for styling.
   *      dateFormat  - on datatime fields you can pass string formatting (e.g. 'd/m/Y' or drupal formats like 'medium')
   */
  public function get($iStart = NULL, $options = []){

    foreach ($options as $key => $var) {
      $$key = $var;
    }

    if (!isset($outputType))
      $outputType = "rendered";

    if (!isset($class))
      $class = NULL;

    if (!isset($dateFormat))
      $dateFormat = "medium";

    $output         = [];
    $fieldContents  = $this->_raw;
    $fieldType      = $this->_type;

    $iNoOfValues = count($fieldContents);

    if (is_null($iStart)) {
      $iStart = 0;
    } else {
      $iNoOfValues = $iStart;
    }

    for ($idx = $iStart; $idx < $iNoOfValues; $idx++) {

      switch($fieldType) {
        case 'email':
          $element = $this->getEmailLink($idx, $outputType);
          break;

        case 'file':
          $element = $this->getFile($idx, $outputType);
          break;

        case 'file_uri':
          $element = $this->getFileURI($idx, $outputType);
          break;

        case 'link':
          $element = $this->getUrlLink($idx, $outputType);
          break;

        case 'entity_reference':
          $element = $this->getEntityLink($idx);
          break;

        case 'entity_reference_revisions':
          $element = $this->getEntityLink($idx);
          break;

        case 'text_with_summary':
          $element = $fieldContents[$idx]["value"]; //((!empty($output)) ? ', ' : '') .
          break;

        case 'image':
          //Look at drupal Render function here....
          $element = $this->getImage($idx, "large", $outputType);
          break;

        case 'created':
          $element = \Drupal::service('date.formatter')
                        ->format(date($fieldContents[$idx]["value"]), 'custom', $dateFormat);
          break;

        case 'datetime':
          $dateString = $fieldContents[$idx]["value"];
          $dateTime = date('Y-M-d H:i:s e', strtotime($dateString));
          $element = \Drupal::service('date.formatter')
                          ->format(strtotime($dateString), 'custom', $dateFormat);
          break;

        case 'list_string':
          $element = $this->getLookupValue($idx, $outputType);
          break;

        case 'path':
          $element = ($outputType != 'rendered') ? $fieldContents[$idx][$outputType] : $fieldContents[$idx]["source"];
          break;

        default:
          $element = $fieldContents[$idx]["value"]; //((!empty($output)) ? ', ' : '') .
      }

      array_push($output, $element);
    }

    return implode(', ', $output) ;
  }

  /*
   * Function that returns the contents of a image field
   * * @param int $idx
   *    [Optional] Pass through the index of the field value
   * * @param string $style
   *    [Optional] Pass through the style of image to be rendered
   * * @param string $outputType default = "rendered"
   *    [Optional] Pass through the output style of image rendered html or image Uri
   * * @param string $class
   *    [Optional] When using rendered output you can pass though classes for styling.
   */
  public function getImage($idx = 0, $style = "large", $outputType = "rendered", $class = NULL){

    $fieldContents = $this->_raw;
    $fieldType     = $this->_type;

    if ($idx >= count($fieldContents))
    {
        return ''; //'Unexpected Index '.$idx;
    }

    if ($fieldType == 'image') {

      $imageEntity = File::load($fieldContents[$idx]["target_id"]);

      $this->_properties = array(
        'style_name' => $style,
        'uri' => $imageEntity->getFileUri(),
      );

      // The image.factory service will check if our image is valid.
      $image = \Drupal::service('image.factory')->get($this->_properties['uri']);

      if ($image->isValid()) {
        $this->_properties['width']   = $image->getWidth();
        $this->_properties['height']  = $image->getHeight();
        $this->_properties['alt']     = $fieldContents[0]['alt'];
      }
      else {
        $this->_properties['width'] = $this->_properties['height'] = NULL;
      }

      $image_render_array = [
        '#theme' => 'image_style',
        '#width' => $this->_properties['width'],
        '#height' => $this->_properties['height'],
        '#style_name' => $this->_properties['style_name'],
        '#uri' => $this->_properties['uri'],
        '#attributes' => [
          'class' => !is_null($class) ? $class : '',
          'alt' => $this->_properties['alt']
        ]
      ];

      if ($outputType == "rendered"){
        $renderer = \Drupal::service('renderer');
        return $renderer->renderRoot($image_render_array);
      }
      else{
        return $this->_properties['uri'];
      }
    } else{
        return 'incorrect node type use function getVal()';
    }
  }

  /*
   * Function that returns the contents of a hyperlink field
   * * @param int $idx
   *    [Optional] Pass through the index of the field value
   * * @param string $style
   *    [Optional] Pass through the style of image to be rendered
   * * @param string $outputType default = "rendered"
   *    [Optional] Pass through the output style of image rendered html or link Uri
   * * @param string $class
   *    [Optional] When using rendered output you can pass though classes for styling.
   * * @param string $target
   *    [Optional] When using rendered output you can pass though target for the link.
   */
  public function getUrlLink($idx = 0, $outputType = "rendered", $class = NULL, $target = "_blank"){

    $fieldContents = $this->_raw;
    $fieldType     = $this->_type;

    if ($idx >= count($fieldContents))
    {
        return ''; //'Unexpected Index '.$idx;
    }

    if ($fieldType == 'link') {
      $class = !is_null($class) ? "class='{$class}'": '';
      $target = !is_null($target) ? "target='{$target}'": '';
      $title = !empty($fieldContents[$idx]['title']) ? "title='Link to {$fieldContents[$idx]['title']}'" : '';

      $link_render_array = [ '#markup' => "<a href='{$fieldContents[$idx]["uri"]}' {$class} {$title} {$target}>{$fieldContents[$idx]["title"]}</a>"];

      if ($outputType == "rendered"){
        $renderer = \Drupal::service('renderer');
        return $renderer->renderRoot($link_render_array);
      }
      else{
        return $fieldContents[$idx]["uri"];
      }
    }
  }

  /*
   * Function that returns the contents of a email link field
   * * @param int $idx
   *    [Optional] Pass through the index of the field value
   * * @param string $style
   *    [Optional] Pass through the style of image to be rendered
   * * @param string $outputType default = "rendered"
   *    [Optional] Pass through the output style of image rendered html or link Uri
   * * @param string $class
   *    [Optional] When using rendered output you can pass though classes for styling.
   * * @param string $target
   *    [Optional] When using rendered output you can pass though target for the link.
   */
  public function getEmailLink($idx = 0, $outputType = "rendered", $class = NULL, $target = "_blank"){

    $fieldContents = $this->_raw;
    $fieldType     = $this->_type;

    if ($idx >= count($fieldContents))
    {
        return ''; //'Unexpected Index '.$idx;
    }

    if ($fieldType == 'email') {

      $class = !is_null($class) ? "class='{$class}'": '';
      $target = !is_null($target) ? "target='{$target}'" : '';
      $title = !empty($fieldContents[$idx]['title']) ? "title='{$fieldContents[$idx]['title']}'" : '';

      $link_render_array = [ '#markup' => "<a href='mailto:{$fieldContents[$idx]["value"]}' {$class} {$title} {$target}>{$fieldContents[$idx]["value"]}</a>"];

      if ($outputType == "rendered"){
        $renderer = \Drupal::service('renderer');
        return $renderer->renderRoot($link_render_array);
      }
      else{
        return $fieldContents[$idx]["value"];
      }
    }
  }

  /*
   * Function that returns the contents of a list lookup field
   * * @param int $idx
   *    [Optional] Pass through the index of the field value
   * * @param string $outputType default = "rendered"
   *    [Optional] Pass through the output style discription or short code
   */
  public function getLookupValue($idx = 0, $outputType = "rendered"){

    $fieldContents = $this->_raw;
    $fieldType     = $this->_type;

    if ($idx >= count($fieldContents)) {
      return ''; //'Unexpected Index '.$idx;
    }

    if ($fieldType == 'list_string') {

      $allowedList = $this->_settings['allowed_values'];

      if ($outputType == "rendered"){
        return $allowedList[$fieldContents[$idx]["value"]];;
      } else {
        return $fieldContents[$idx]["value"];
      }
    }

  }

  /*
   * Function that returns the contents of a file field
   * * @param int $idx
   *    [Optional] Pass through the index of the field value
   * * @param string $outputType default = "rendered"
   *    [Optional] Pass through the output style of file link rendered html or file Uri
   * * @param string $class
   *    [Optional] When using rendered output you can pass though classes for styling.
   * * @param string $target
   *    [Optional] When using rendered output you can pass though target for the link.
   */
  public function getFile($idx = 0, $outputType = "rendered", $class = NULL, $target = "_blank"){

    $fieldContents = $this->_raw;
    $fieldType     = $this->_type;

    if ($idx >= count($fieldContents))
    {
        return ''; //'Unexpected Index '.$idx;
    }

    if ($fieldType == 'file') {

      $targetId = $fieldContents[$idx]["target_id"];
      $fileEntity = File::load($targetId);

      $this->_properties = array(
        'uri' => file_url_transform_relative(file_create_url($fileEntity->getFileUri())),
        'filename' => $fileEntity->getFilename(),
        'description' => $fieldContents[$idx]["description"]
      );

      $class    = !is_null($class) ? "class='{$class}'": "class='download-link {$class}'";
      $target   = !is_null($target) ? "target='{$target}'" : '';
      $title    = !empty($fieldContents[$idx]['title']) ? "title='{$fieldContents[$idx]['title']}'" : '';

      $link_render_array = [ '#markup' => "<div {$class}><a href='{$this->_properties['uri']}' {$title} {$target}>{$this->_properties['filename']}</a> {$this->_properties['description']}</div>"];

      if ($outputType == "rendered"){
        $renderer = \Drupal::service('renderer');
        return $renderer->renderRoot($link_render_array);
      }
      else{
        return $this->_properties['uri'];
      }
    }
  }

  /*
   * Function that returns the contents of a File URI field
   * * @param int $idx
   *    [Optional] Pass through the index of the field value
   * * @param string $outputType default = "rendered"
   *    [Optional] Pass through the output style of file link rendered html or file Uri
   * * @param string $class
   *    [Optional] When using rendered output you can pass though classes for styling.
   * * @param string $target
   *    [Optional] When using rendered output you can pass though target for the link.
   */
  public function getFileURI($idx = 0, $outputType = "rendered", $class = NULL, $target = "_blank") {

    $fieldContents = $this->_raw;
    $fieldType     = $this->_type;

    if ($idx >= count($fieldContents)) {
      return ''; //'Unexpected Index '.$idx;
    }

    if ($fieldType == 'file_uri') {

      $targetId = $fieldContents[$idx]["target_id"];
      $fileEntity = File::load($targetId);

      $this->_properties = array(
        'uri' => file_url_transform_relative(file_create_url($fileEntity->getFileUri())),
        'filename' => $fileEntity->getFilename(),
        'description' => $fieldContents[$idx]["description"]
      );

      $class    = !is_null($class) ? "class='{$class}'" : "class='download-link {$class}'";
      $target   = !is_null($target) ? "target='{$target}'" : '';
      $title    = !empty($fieldContents[$idx]['title']) ? "title='{$fieldContents[$idx]['title']}'" : '';

      $link_render_array = ['#markup' => "<div {$class}><a href='{$this->_properties['uri']}' {$title} {$target}>{$this->_properties['filename']}</a> {$this->_properties['description']}</div>"];

      if ($outputType == "rendered") {
        $renderer = \Drupal::service('renderer');
        return $renderer->renderRoot($link_render_array);
      } else {
        return $this->_properties['uri'];
      }
    }
  }

  /*
   * Function that returns the contents of a linked entity field
   * * @param int $idx
   *    [Optional] Pass through the index of the field value
   */
  public function getEntityLink($idx = 0){

    $fieldContents = $this->_raw;
    $fieldType     = $this->_type;

    if ($idx >= count($fieldContents))
    {
        return ''; //'Unexpected Index '.$idx;
    }

    if ($fieldType == 'entity_reference' || $fieldType == 'entity_reference_revisions') {

      $targetId = $fieldContents[$idx]["target_id"];

      $settings = $this->_settings;

      if ($settings['target_type'] == 'taxonomy_term') {
        //if linked field type is taxonomy_term then load taxonomy term node
        $node = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($targetId);
      } else if ($settings['target_type'] == 'user') {
        //if linked field type is User then load user entity node
        $node = \Drupal\user\Entity\User::load($targetId);
      } else if ($settings['target_type'] == 'paragraph') {
        //if linked field type is User then load user entity node
        $node = \Drupal\paragraphs\Entity\Paragraph::load($targetId);
      } else {
        //else load defult node
        $node = \Drupal::entityTypeManager()->getStorage('node')->load($targetId);
      }

      //retun default name depended on available function
      if (method_exists($node, 'getName')) {
        return $node->getName();
      } else if (method_exists($node, 'getDisplayName')){
        return $node->getDisplayName();
      } else if (method_exists($node, 'getTitle')){
        return $node->getTitle();
      } else if ($settings['target_type'] == 'paragraph') {
        //dump($node);

        if (method_exists($node, 'getFields')) {
          $nodeFields = $node->getFields(false);
          $FieldList = "";

          foreach ($nodeFields as $fieldId => $field) {
            $FieldList = $FieldList . $fieldId . ", ";
          }

          return "Please specifiy a child field: ". $FieldList;
        }
      } else {
        dump("no get defined method - check object and expand ouput methods");
//        dump($node);
      }
    }
  }

  /*
   * Function that used for debugging field strutures.
   */
  public function getDebug(){
//    dump($this);
  }

  private function createProperty($name, $value) {
    $this->{$name} = $value;
  }

  /*
   * Private Function that checks to see if a field has child fields (e.g. entity_reference fields)
   */
  private function hasChildEntities() {
    $fieldContents = $this->_raw;
    $fieldType     = $this->_type;

    if ($fieldType == 'entity_reference' || $fieldType == 'entity_reference_revisions') {
      return true;
    }

    return false;
  }

  /*
   * Private Function that discovers child entities of a field (e.g. entity_reference fields)
   */
  private function discoverChildEntities(){
    //discover child entities...
    $fieldContents = $this->_raw;
    $targetType    = $this->_settings['target_type'];

    foreach ($fieldContents as $idx => $fieldValue) {
      if(isset($fieldValue['target_id'])){
        $targetId = $fieldValue["target_id"];
        $node = \Drupal::entityTypeManager()->getStorage($targetType)->load($targetId);

        if (method_exists($node, 'getFields')) {
          $nodeFields = $node->getFields();
          foreach ($nodeFields as $fieldId => $field) {
            if ($node->hasField($fieldId)){
              $fluidFieldValue = new fluidField($node->get($fieldId));
              $this->createProperty($fieldId, $fluidFieldValue);
            }
          }
        }
      }
    }
  }

  public function __construct($drupalObject){
    $this->_raw = $drupalObject->getValue();
    $this->_type = $drupalObject->getFieldDefinition()->getType();
    $this->_settings = $drupalObject->getSettings();

    if ($this->hasChildEntities()){
     $this->discoverChildEntities();
    }
  }
}










/* A custom wrapper class that represnts all fields in a row on
 * a view with helper functions to simplfy usage in twig */
class fluidRow {
  public function createProperty($name, $value)
  {
    $this->{$name} = $value;
  }
}


