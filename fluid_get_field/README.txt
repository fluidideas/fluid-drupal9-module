
Documentation for the Fluid Get Field module
____________________________________________

This module uses various preprocess functions to simplify the way that
Drupal's objects (nodes and views) and their fields are accessed in
Twig files.

It works by creating a fluid variable in certain twig files, namely:

- page.html.twig
- views-VIEW-FORMATTER.html.twig
- views-VIEW-FORMATTER--FIELD_NAME.html.twig

This includes the various twig files derived from these such as page--node--76.html.twig

You can examine the contents of the fluid variable by enabling the Devel Kint module and
adding the below code to one of the above files

{{ kint(fluid) }}

You'll then be given an object that varies depending on the Drupal object it was generated from

PAGE.HTML.TWIG
______________

The fluid object only contains data if the page is a node. If it is the fluid object will contain an array
of the fields on that node. You can kint that field and see the data structure on it however for the most part
you'll just need to use the get() function on the field such as:

{{ fluid.field_category.get() }}

This will return the rendered value of the field. In the case of entity references such as taxonomy terms, node
references or paragraph items you can access the child field in the same way such as:

{{ field.field_category.field_logo.get() }}

VIEWS-VIEW-FORMATTER.HTML.TWIG
______________________________

The fluid object will contain an array of rows with their fields (as selected in the view config) accessible as
follows:

{% for row in fluid.rows %}
  {{ row.body.get() }}
{% endfor %}

VIEWS-VIEW-FORMATTER--FIELD.HTML.TWIG
_____________________________________

The fluid object will contain an array of rows with their fields (as selected in the view config) accessible as
follows:

{% for row in fluid.rows %}
  {{ row.body.get() }}
{% endfor %}

