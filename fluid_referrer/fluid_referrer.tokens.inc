<?php
use Drupal\Core\Render\BubbleableMetadata;

//create token group and name
function fluid_referrer_token_info(){
  
  $types['fluid'] = array(
    'name' => t('Fluid'),
    'description' => t('Fluid module')
  );	
  $tokens['referrer'] = array(
    'name' => t('Referrer'),
    'description' => t('Define enquiry referral')
  );

  return array(
    'types' => $types,
    'tokens' => array(
      'fluid' => $tokens
    )
  );	
}


//implement tokens
function fluid_referrer_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata){
  $replacements = array();
  
  //check for token type defined above
  if($type == 'fluid') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'referrer':
          if(isset($_COOKIE['marketing_source'])){
            $replacements[$original] = "Referral Source: ".$_COOKIE['marketing_source'];
          }else{
            $replacements[$original] = "No cookie set";
          }
          break;
      }
    }
  }
  return $replacements;
}
?>