<?php

namespace Drupal\fluid_referrer\Element;

use Drupal\Core\Render\Element\Hidden;

/**
 * Provides a hidden form element for the fluid webform parent node element
 *
 * @FormElement("fluid_referrer")
 */
class FluidReferrerReferralElement extends Hidden {

}
