<?php

namespace Drupal\fluid_referrer\Plugin\WebformElement;

use Drupal\webform\WebformInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\TextBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'parent node' element which grabs the node id of the page your on.
 *
 * @WebformElement(
 *   id = "fluid_referrer",
 *   api = "https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!Element!Hidden.php/class/Hidden",
 *   label = @Translation("Referrer source"),
 *   description = @Translation("Renders referrer Cookie content on in webform results"),
 *   category = @Translation("Basic elements"),
 * )
 */
class FluidReferrerReferralElement extends TextBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    return [
      // Element settings.
      'title' => '',
      'prepopulate' => FALSE
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preview() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTestValues(array $element, WebformInterface $webform, array $options = []) {
    // Hidden elements should never get a test value.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Remove the default section under the advanced tab.
    unset($form['default']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    $value = "No Cookies";
    if(isset($_COOKIE['marketing_source'])){
      $value = $_COOKIE['marketing_source'];
    }else{
      $media = getReferrerMediaName();
      $value = $media['type']." / ".$media['value'];
    }
    $element['#value'] = $value; // set the node id here
    parent::prepare($element, $webform_submission);
  }

}
