## WIP
This package is still a work in progress the following modules have been updated to D9:

* fluid_blocks_ref
* fluid_users
* fluid_list


This list is still to do:

* fluid_advagg
* fluid_get_field
* fluid_leaderboard
* fluid_list
* fluid_mail
* fluid_mailchimp
* fluid_menu
* fluid_paragraphs
* fluid_paragraphs_additional
* fluid_referrer
* fluid_webform
* fluid_webp


# About

Fluid Drupal 9 custom modules

## Installation
Add to composer via the repositories
```
...
"repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:fluidideas/fluid-drupal9-module.git"
        },
],
...
```
