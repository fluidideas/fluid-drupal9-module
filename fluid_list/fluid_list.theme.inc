<?php

use Drupal\Core\Template\Attribute;

/**
 * @file
 * Theme for Fluid List view.
 */
function template_preprocess_views_view_fluid_list(&$variables) {

  $view = $variables['view'];
  $rows = $variables['rows'];
  $style = $view->style_plugin;
  $options = $style->options;

  $variables['default_row_class'] = !empty($options['default_row_class']);
  foreach ($rows as $id => $row) {
    $variables['rows'][$id] = [];
    $variables['rows'][$id]['content'] = $row;
    $variables['rows'][$id]['attributes'] = new Attribute();
    if ($row_class = $view->style_plugin->getRowClass($id)) {
      $variables['rows'][$id]['attributes']->addClass($row_class);
    }
  }

  // Update options for twig.
  $variables['options'] = $options;

}

function fluid_list_preprocess_views_view_fields(&$variables){

  $view = $variables['view'];
  $style = $view->style_plugin;
  $options = $style->options;
  if(isset($variables['row']->nid)){
    $variables['link_url'] = \Drupal::service('path_alias.manager')->getAliasByPath('/node/'.$variables['row']->nid);
  }
  $variables['wrap_link'] = !empty($options['wrap_link']);

  
  foreach($variables['fields'] as $key => $val){
    if(is_object($variables['row']->_entity)){
      if(isset($val->handler->definition['field_name'])){ // check if you can find a field definition else it's a custom text/view link
        $type = $variables['row']->_entity->get($key)->getFieldDefinition()->getType();
        if($type == 'image'){
           // if field is an image field, add to image wrapper, remove from content wrapper
           $variables['image_fields'][$key] = $val;
           unset($variables['fields'][$key]);
        }
      }
    }
  }

}
