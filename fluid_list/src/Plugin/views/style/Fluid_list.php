<?php

namespace Drupal\fluid_list\Plugin\views\style;

use Drupal\core\form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "fluid_list",
 *   title = @Translation("Fluid List"),
 *   help = @Translation("Render a list of years and months in reverse chronological order linked to content."),
 *   theme = "views_view_fluid_list",
 *   display_types = { "normal" }
 * )
 */
class Fluid_list extends StylePluginBase {

   /**
   * Does this Style plugin allow Row plugins?
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['path'] = array('default' => 'fluid_list');
    $options['wrap_link'] = array('default' => 1);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Extra CSS classes.
    $form['wrapperclasses'] = array(
      '#type' => 'textfield',
      '#title' => t('Wrapper classes'),
      '#default_value' => (isset($this->options['wrapperclasses'])) ? $this->options['wrapperclasses'] : 'view-fluid-lists flex wrap',
      '#description' => t('Add css classes to wrap around the whole view'),
    );
    $form['wrap_link'] = array(
      '#type' => 'select',
      '#options' => array(0 => 'Don\'t wrap the row in a link', 1 => 'Wrap all fields in a link to the content'),
      '#title' => t('Wrap each row in a link'),
      '#description' => t('Tick this box to wrap a link around the entire row'),
      '#default_value' => $this->options['wrap_link'],
    );
  }

}
