<?php

namespace Drupal\fluid_menu\TwigExtension;

use Drupal\Core\Menu\MenuTreeParameters;

/**
 * extend Drupal's Twig_Extension class
 */
class FluidMenuTwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   * Let Drupal know the name of your extension
   * must be unique name, string
   */
  public function getName() {
    return 'fluid_menu.FluidMenuTwigExtension';
  }

  /**
   * {@inheritdoc}
   * Return your custom twig function to Drupal
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('fluid_menu_load', [$this, 'fluid_menu_load'])
    ];
  }

  public static function fluid_menu_load($parent = NULL) {
    $menu_name = 'main';
    $menu_tree = \Drupal::menuTree();
    $parameters = new MenuTreeParameters();
    $parameters->setMinDepth(0);
    $parameters->onlyEnabledLinks();

    $tree = $menu_tree->load($menu_name, $parameters);
    $manipulators = array(
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    $tree = $menu_tree->transform($tree, $manipulators);
    $list = [];

    $menuArray = [];

    foreach($tree as $parentItem) {
      $parentId = $parentItem->link->getDerivativeId();
      $menuArray[$parentId] = [
        'mlid' => $parentItem->link->getDerivativeId(),
        'active' => $parentItem->inActiveTrail,
        'title' => $parentItem->link->getTitle(),
        'url' => $parentItem->link->getUrlObject()->toString(),
        'description' => $parentItem->link->getDescription(),
      ];
      if(!empty($parentItem->subtree)){
        foreach($parentItem->subtree as $childItem){
          $menuArray[$parentId]['children'][$childItem->link->getDerivativeId()] = [
            'active' => $childItem->inActiveTrail,
            'title' => $childItem->link->getTitle(),
            'url' => $childItem->link->getUrlObject()->toString(),
            'description' => $childItem->link->getDescription(),
          ];
          if(!empty($childItem->subtree)){
            foreach($childItem->subtree as $grandChildItem){
              $menuArray[$parentId]['children'][$childItem->link->getDerivativeId()]['children'][] = [
                'active' => $grandChildItem->inActiveTrail,
                'title' => $grandChildItem->link->getTitle(),
                'url' => $grandChildItem->link->getUrlObject()->toString(),
                'description' => $grandChildItem->link->getDescription(),
              ];
            }
          }
        }
      }
    }

    if($parent){
      return isset($menuArray[$parent]) ? $menuArray[$parent] : FALSE;
    }
    return $menuArray;
  }

}
