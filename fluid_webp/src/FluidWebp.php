<?php

namespace Drupal\fluid_webp;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\file\Entity\File;
use Drupal\fluid_webp\Controller\ImageStyleDownloadController;

/**
 * Class Webp.
 *
 * @package Drupal\fluid_webp
 */
class FluidWebp {

  use StringTranslationTrait;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Default image processing quality.
   *
   * @var int
   */
  protected $defaultQuality;

  /**
   * Webp constructor.
   *
   * @param \Drupal\Core\Image\ImageFactory $imageFactory
   *   Image factory to be used.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger channel factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   String translation interface.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration factory.
   */
  public function __construct(ImageFactory $imageFactory, LoggerChannelFactoryInterface $loggerFactory, TranslationInterface $stringTranslation, ConfigFactoryInterface $configFactory) {
    $this->imageFactory = $imageFactory;
    $this->logger = $loggerFactory->get('fluid_webp');
    $this->setStringTranslation($stringTranslation);
    $this->defaultQuality = 75;
  }

  /**
   * Creates a WebP copy of a source image URI.
   *
   * @param string $uri
   *   Image URI.
   * @param int $quality
   *   Image quality factor (optional).
   *
   * @return bool|string
   *   The location of the WebP image if successful, FALSE if not successful.
   */
  public function createWebpCopy($uri, $quality = NULL) {
    $webp = FALSE;

    // Generate a GD resource from the source image. You can't pass GD resources
    // created by the $imageFactory as a parameter to another function, so we
    // have to do everything in one function.
    $sourceImage = $this->imageFactory->get($uri, 'gd');
    /** @var \Drupal\system\Plugin\ImageToolkit\GDToolkit $toolkit */
    $toolkit = $sourceImage->getToolkit();
    $mimeType = $sourceImage->getMimeType();
    $sourceImage = $toolkit->getResource();

    // If we can generate a GD resource from the source image, generate the URI
    // of the WebP copy and try to create it.
    if ($sourceImage !== NULL) {// && $mimeType !== 'image/png') {

      $pathInfo = pathinfo($uri);
      $destination = strtr('@directory/@filename.webp', [
        '@directory' => $pathInfo['dirname'],
        '@filename' => $pathInfo['filename'],
        '@extension' => $pathInfo['extension'],
      ]);

      if (is_null($quality)) {
        $quality = $this->defaultQuality;
      }

      if (@imagewebp($sourceImage, $destination, $quality)) {
        // In some cases, libgd generates broken images. See
        // https://stackoverflow.com/questions/30078090/imagewebp-php-creates-corrupted-webp-files
        // for more information.
        if (filesize($destination) % 2 == 1) {
          file_put_contents($destination, "\0", FILE_APPEND);
        }

        @imagedestroy($sourceImage);
        $webp = $destination;
      }
      else {
        $error = $this->t('Could not generate WebP image.');
        $this->logger->error($error);
      }
    }
    // If we can't generate a GD resource from the source image, fail safely.
    else {
      $error = $this->t('Could not generate image resource from URI @uri.', [
        '@uri' => $uri,
      ]);
      $this->logger->error($error);
    }

    return $webp;
  }

  /**
   * Deletes all image style derivatives.
   */
  public function deleteImageStyleDerivatives() {
    // Remove the styles directory and generated images.
    if (@!file_unmanaged_delete_recursive(file_default_scheme() . '://styles')) {
      $error = $this->t('Could not delete image style directory while uninstalling WebP. You have to delete it manually.');
      $this->logger->error($error);
    }
  }

  /**
   * Receives the path of an image and returns the webp equivalent.
   *
   * @param $filepath
   *   Filepath to convert into .webp extension
   *
   * @return string
   *  file string with .webp extension
   */
  public function getWebpFilename($filepath) {
    if(strpos($filepath, ' 1x, ') !== FALSE){
      $split_paths = explode(' 1x, ', $filepath);
      $parts = parse_url($split_paths[0]);
      $path_parts = pathinfo($parts['path']);
      $parts['path'] = \Drupal\fluid_webp\Controller\ImageStyleDownloadController::str_lreplace('.'.$path_parts['extension'], '.webp', $parts['path']);
      $webp_url = $this->unparse_url($parts);
      $parts = parse_url($split_paths[1]);
      $path_parts = pathinfo($parts['path']);
      $parts['path'] = \Drupal\fluid_webp\Controller\ImageStyleDownloadController::str_lreplace('.'.$path_parts['extension'], '.webp', $parts['path']);
      $webp_url .= ' 1x, '.$this->unparse_url($parts);
    } else {
      $parts = parse_url($filepath);
      $path_parts = pathinfo($parts['path']);
      $parts['path'] = \Drupal\fluid_webp\Controller\ImageStyleDownloadController::str_lreplace('.'.$path_parts['extension'], '.webp', $parts['path']);
      $webp_url = $this->unparse_url($parts);
    }
    return $webp_url;
  }

  private function unparse_url($parsed_url) {
    $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
    $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
    $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
    $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
    $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
    $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
    return $scheme.$host.$port.$path.$query.$fragment;
  }

  public static function generatePicture($filepath, $style) {
    $variables = [
        'responsive_image_style_id' => $style,
        'uri' => $filepath,
    ];
    $image = \Drupal::service('image.factory')->get($filepath);
    if ($image->isValid()) {
        $variables['width'] = $image->getWidth();
        $variables['height'] = $image->getHeight();
    } else {
        $variables['width'] = $variables['height'] = NULL;
    }
    $image_build = [
        '#theme' => 'responsive_image',
        '#width' => $variables['width'],
        '#height' => $variables['height'],
        '#responsive_image_style_id' => $variables['responsive_image_style_id'],
        '#uri' => $variables['uri'],
    ];
    $content = \Drupal::service('renderer')->render($image_build);
    return $content;
  }

}
